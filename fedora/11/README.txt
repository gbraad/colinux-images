Fedora 11 base system for use with coLinux.


This archive contains a Fedora 11 installation which can be used with
coLinux. Included are the scripts to start the emulation environment.

The login for the environment is

   username: root
   password:

Network is configured from the parameters file 'fedora.conf'.

Installation was performed using the installation DVD in basic mode on
qemu. It has been tested on the stable version 0.7.3 of coLinux and 
winpcap on Windows XP.

Any comments are welcome at:

   me+colinux@gbraad.nl
   gbraad@fedoraproject.org


Gerard Braad
